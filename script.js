/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
- createElement("div")
- textContent 
- innerHTML

2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
- Знайти елемент. querySelector/getElements
- Видалити елемент. .remove()
- Перевірте зміни.

3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
- insertAdjacentElement
1 beforebegin
2 afterbegin
3 beforeend
4 afterend
- 

Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".

 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.

 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.

 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.

 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

const link = document.createElement("a");
link.textContent = "Learn More";
link.setAttribute("href", "#");

const footer = document.querySelector("footer");
const firstParagraph = footer.querySelector("p");
footer.insertBefore(link, firstParagraph.nextSibling);

// ___________________________________

const selectElement = document.createElement("select");
selectElement.id = "rating";

const mainElement = document.querySelector("main");
const featuresSection = document.querySelector("#features");

mainElement.insertBefore(selectElement, featuresSection);

const selectElement1 = document.getElementById("rating");
const selectElement2 = document.getElementById("rating");
const selectElement3 = document.getElementById("rating");
const selectElement4 = document.getElementById("rating");

const optionElement1 = document.createElement("option");
const optionElement2 = document.createElement("option");
const optionElement3 = document.createElement("option");
const optionElement4 = document.createElement("option");

optionElement1.value = "1";
optionElement1.textContent = "1 Star";

optionElement2.value = "2";
optionElement2.textContent = "2 Stars";

optionElement3.value = "3";
optionElement3.textContent = "3 Stars";

optionElement4.value = "4";
optionElement4.textContent = "4 Stars";

selectElement1.append(optionElement1);
selectElement2.append(optionElement2);
selectElement3.append(optionElement3);
selectElement4.append(optionElement4);

// const selectElement = document.createElement("select");

// selectElement.id = "rating";

// const ratings = [
//   { value: "1", text: "1 Star" },
//   { value: "2", text: "2 Stars" },
//   { value: "3", text: "3 Stars" },
//   { value: "4", text: "4 Stars" },
// ];

// ratings.forEach((rating) => {
//   const optionElement = document.createElement("option");
//   optionElement.value = rating.value;
//   optionElement.text = rating.text;
//   selectElement.appendChild(optionElement);
// });

// const mainElement = document.querySelector("main");

// const featuresSection = document.querySelector("#features");

// mainElement.insertBefore(selectElement, featuresSection);
